/*------- variables for odd rows' background -------*/
var tableRowContentDOM = document.querySelectorAll('.b-table__row_content');
var tableRowContent = [];
/*--------------- variables fot TABs ---------------*/
var departureCheckBox = document.querySelector('.b-radiobuttons__input_departure'),
    arriveCheckBox = document.querySelector('.b-radiobuttons__input_arrive'),
    allCheckBox = document.querySelector('.b-radiobuttons__input_all');
var departureLabel = document.querySelector('.b-radiobuttons__radio-label_departure'),
    arriveLabel = document.querySelector('.b-radiobuttons__radio-label_arrive'),
    allLabel = document.querySelector('.b-radiobuttons__radio-label_all');
var departure = document.querySelectorAll('.b-flight-departure'),
    arrival = document.querySelectorAll('.b-flight-arrival'),
    all = document.querySelectorAll('.b-table__row_content');
/*-------------- variables fot pop-up --------------*/
var popUp = document.querySelector('.b-popUp-outer'),
    tmpCell = document.querySelectorAll('.b-table__cell_tmp-cont'),
    clickedParent = document.querySelector('.b-table__body'),
    PopUpOuter = document.querySelector('.b-popUp-outer'),
    PopUpInner = document.querySelector('.b-popUp-inner'),
    clickedPopUp = document.querySelector('.b-popUp-close');
/*-------------------subfunctions -------------------*/
    function oddTableRowBackground(){
        var j = 0;
        for(var i = 0; i<tableRowContentDOM.length; i++){
            if(!tableRowContentDOM[i].style.display){
                tableRowContent[j] = tableRowContentDOM[i];
                tableRowContent[j].classList.remove('b-table__row_darkBG');
                    if(!(j%2)){ 
                        tableRowContent[j].classList.add('b-table__row_darkBG');
                    }
                j++;
            }
        }
    }
    function defaultView(){
        for(var i=0; i<all.length; i++){
            all[i].style.display = '';
        }
    }
    function LabelClassToggle(x, y, z){
        x.classList.add('b-radiobuttons__radio-label_active');
        x.classList.remove('b-radiobuttons__radio-label_passive');
        y.classList.remove('b-radiobuttons__radio-label_active');
        y.classList.add('b-radiobuttons__radio-label_passive');
        z.classList.remove('b-radiobuttons__radio-label_active');
        z.classList.add('b-radiobuttons__radio-label_passive');
    }
/*------------------------ end ------------------------*/
    oddTableRowBackground();
/*------------------ for column hover ------------------*/
    var cellHover = document.querySelector('.b-table__body'),
        cellHoverCollection;
    cellHover.onmouseover = function(event){
        var el = event.target;
        while(!el.hasAttribute('data-cell')){
            el = el.parentElement;
        };
        var AttrName = el.getAttribute('data-cell');
        cellHoverCollection = document.querySelectorAll('[data-cell=' + AttrName + ']');
        for(var i=0; i<cellHoverCollection.length; i++){
            cellHoverCollection[i].style.backgroundColor = 'rgba(0, 0, 0, .2)';
        }
    }
    cellHover.onmouseout = function(event){
        for(var i=0; i<cellHoverCollection.length; i++){
                cellHoverCollection[i].style.backgroundColor = '';
        }
    }
/*------------------ for tabs -------------------*/
    departureCheckBox.onchange = function(){
        defaultView();
        if(departureCheckBox.checked){
            LabelClassToggle(departureLabel, arriveLabel, allLabel)
            for(var i=0; i<arrival.length; i++){
                arrival[i].parentElement.parentElement.style.display = 'none';
            }
            oddTableRowBackground();
        }
    };
    arriveCheckBox.onchange = function(){
        defaultView();
        if(arriveCheckBox.checked){
            LabelClassToggle(arriveLabel, departureLabel, allLabel)
            for(var i=0; i<departure.length; i++){
                departure[i].parentElement.parentElement.style.display = 'none';
            }
            oddTableRowBackground();
        }
    };
    allCheckBox.onchange = function(){
        defaultView();
        if(allCheckBox.checked){
            LabelClassToggle(allLabel, departureLabel, arriveLabel)
        }
        oddTableRowBackground();
    }
/*------------------- for pop-up -------------------*/
    clickedParent.onclick = function(event) {
        var target = event.target,
            aim = target.closest('.b-table__row_content'),
            count = aim.children.length,
            content = [];
        for(var i = 0; i<count-1; i++){
            content[i] = aim.children[i].innerHTML;
            tmpCell[i].innerHTML = content[i];
        }
        popUp.style.display = 'block';
        PopUpInner.style.marginTop = (document.body.offsetHeight - PopUpInner.offsetHeight)/2 + 'px';
    }
    clickedPopUp.onclick = function(){
        PopUpOuter.style.display = '';
    }